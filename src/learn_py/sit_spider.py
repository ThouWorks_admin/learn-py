# 爬取新闻网上的新闻标题
import requests
from bs4 import BeautifulSoup


def main():
    path = '2210350121.txt'
    url = 'https://www.sit.edu.cn/xww/xxyw.htm'

    response = requests.get(url)
    response.encoding = 'utf-8'
    soup = BeautifulSoup(response.text, 'html.parser')

    news = soup.find('div', class_='imgList')
    news_list = news.find_all('span', class_='tit') # type: ignore

    with open(path, 'w', encoding='utf-8') as f:
        for news_item in news_list:
            f.write(''.join([news_item.text, '\n']))


if __name__ == '__main__':
    main()
