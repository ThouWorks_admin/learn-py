import os


def main():
    path = os.getcwd()
    for root, dirs, files in os.walk(path):
        print(f'root: {root}')
        print(f'dirs: {dirs}')
        print(f'files: {files}')
        print()


if __name__ == '__main__':
    main()
