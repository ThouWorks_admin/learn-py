"""
编写函数，判断一个数是否为水仙花数。调用函数，输出1000以内的所有水仙花数。提示：水仙花数是一个3位数，它每位上的数字的3次幂之和等于它本身。
"""


def is_narcissistic_number(num):
    """
    判断一个数是否为水仙花数
    """
    num_str = str(num)
    num_len = len(num_str)
    sum = 0
    for i in range(num_len):
        sum += int(num_str[i]) ** num_len
    return sum == num


if __name__ == "__main__":
    for i in range(100, 1000):
        if is_narcissistic_number(i):
            print(i, end="\t")
    print()
