"""
理解格式化输出，并且按照下列要求写出程序：
输入一句话，按照以下格式输出：
例如：输入“Good morning，everyone”
输出格式为：
+---------------------------------------------+
	| Good morning，everyone|  
+---------------------------------------------+
"""


def main():
    offest = 8
    s = input("请输入一句话：")

    print("+" + "-" * (len(s) + offest) + "+")
    print(f"   | {s} |")
    print("+" + "-" * (len(s) + offest) + "+")


if __name__ == "__main__":
    main()
