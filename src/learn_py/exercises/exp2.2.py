"""
编写程序，将由1、2、3、4这四个数字组成的每位数都不相同的所有三位数存入一个列表中并输出该列表。
请利用列表推导式解决该问题。
"""

if __name__ == "__main__":
    list1 = [x * 100 + y * 10 + z for x in range(1, 5) for y in range(
        1, 5) for z in range(1, 5) if x != y and y != z and x != z]
    print(list1)
