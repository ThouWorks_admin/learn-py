"""
编写函数，判断一个数是否为素数。调用该函数，判断从键盘输入的数是否为素数。
"""


def is_prime(num):
    if num < 2:
        return False
    for i in range(2, int(num ** 0.5) + 1):
        if num % i == 0:
            return False
    return True


if __name__ == "__main__":
    user_input = int(input("Enter a number: "))

    if is_prime(user_input):
        print(f"{user_input} is a prime number.")
    else:
        print(f"{user_input} is not a prime number.")
