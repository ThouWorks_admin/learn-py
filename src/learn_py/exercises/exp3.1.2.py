"""
做一个函数，可计算n！，并依次输出1~20的阶乘。
"""


def factorial(n):
    if n == 1:
        return 1
    return n * factorial(n - 1)


if __name__ == "__main__":
    for i in range(1, 21):
        print(f"{i}! = {factorial(i)}")
