"""
编写程序，百钱买百鸡：
一只公鸡5块钱，一只母鸡3块钱，三只小鸡1块钱，
现在要用一百块钱买一百只鸡，
问公鸡、母鸡、小鸡各多少只？
请利用元组推导式解决该问题。
"""

if __name__ == "__main__":
    result = [(x, y, 100 - x - y) for x in range(21) for y in range(
        34) if 5 * x + 3 * y + (100 - x - y) // 3 == 100]
    for x, y, z in result:
        print(f"公鸡{x}只，母鸡{y}只，小鸡{z}只。")
