"""
编写程序，随机生成50个介于[1, 20]之间的整数，然后统计每个整数出现频率。
"""

import random
from pprint import pprint

if __name__ == "__main__":
    list1 = [random.randint(1, 20) for _ in range(50)]
    dict1 = {x: list1.count(x) for x in range(1, 21)}
    pprint(dict1)
