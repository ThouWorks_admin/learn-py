"""
编写程序，某企业为职工发放奖金：
如果入职超过5年，且销售业绩超过15000元的员工，奖金比例为0.2；
销售业绩超过10000元的员工，奖金比例为0.15；
销售业绩超过5000元的员工，奖金比例为0.1；
其他奖金比例为0.05。
如果是入职不超过5年，且销售业绩超过4000的员工，奖金比例为0.045；
否则为0.01。
输入入职年限、销售业绩，输出奖金比例、奖金，并将奖金存放到列表中并输出该列表。
入职年限（为整数）输入-1的时候结束输入。
为了简化，所有输入均假定正确，不需判断小于0的情况。
奖金为销售业绩与奖金比例的乘积。
"""

if __name__ == "__main__":
    bonuses = []
    while True:
        years = int(input("请输入入职年限："))
        if years == -1:
            break

        sales = int(input("请输入销售业绩："))
        if years > 5:
            if sales > 15000:
                bonus_rate = 0.2
            elif sales > 10000:
                bonus_rate = 0.15
            elif sales > 5000:
                bonus_rate = 0.1
            else:
                bonus_rate = 0.05
        else:
            if sales > 4000:
                bonus_rate = 0.045
            else:
                bonus_rate = 0.01
        
        bonus = sales * bonus_rate
        bonuses.append(bonus)
        print(f"奖金比例：{bonus_rate}，奖金：{bonus}")
    print(bonuses)
