"""
字典练习，建立一个字典，包含以下信息，输入姓名，通过字典查到该人的信息，并且按照以下格式输出：
Tom
num：13456
addr：Foo street 45
姓名	电话号码	住址
Tom	123456	Foo street 45
Lily	456789	Bar street  23
Jack	789123	5th street 56
"""

info = {
    'Tom': {'num': 13456, 'addr': 'Foo street 45'},
    'Lily': {'num': 456789, 'addr': 'Bar street 23'},
    'Jack': {'num': 789123, 'addr': '5th street 56'}
}


def main():
    name = input('请输入姓名：')
    print('姓名\t电话号码\t住址')
    print(f"{name}\t{info[name]['num']}\t{info[name]['addr']}")


if __name__ == '__main__':
    main()
