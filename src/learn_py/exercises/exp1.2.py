"""
按照以下格式打印水果价格表：
===================
Item                    Price
——————————————————
Apples                  0.4
Pears                    0.5
Cantaloupes           1.92
====================
"""

if __name__ == "__main__":
    fruits = {
        "Apples": 0.4,
        "Pears": 0.5,
        "Cantaloupes": 1.92
    }

    print("=" * 20)
    print("Item".ljust(20), "Price")
    print("-" * 20)

    for k, v in fruits.items():
        print(k.ljust(20), str(v).rjust(5))

    print("=" * 20)
