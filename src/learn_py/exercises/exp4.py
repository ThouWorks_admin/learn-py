"""
1.创建SchoolMem类，该类中包含三种属性：姓名、性别、年龄以及针对每个属性的get和set方法； 
2.创建Student类，继承自SchoolMem类，添加额外三个属性：班级、学号和数量统计。在Student类中，设置类变量count。统计全体学生总数。
3.创建Teacher类，继承自SchoolMem类，添加额外三个属性：科室、工号和数量统计。在Teacher类中，设置类变量count。统计全体学生总数。
4.要求在Student类和Teacher类中分别实现printInfo方法，该方法打印对象的所有属性信息。创建输出学生总数，教师总数的方法。
"""


class SchoolMem:
    def __init__(self):
        self.name = ''
        self.gender = ''
        self.age = ''

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_gender(self):
        return self.gender

    def set_gender(self, gender):
        self.gender = gender

    def get_age(self):
        return self.age

    def set_age(self, age):
        self.age = age


class Student(SchoolMem):
    count = 0

    def __init__(self):
        super().__init__()
        self.class_ = ''
        self.stu_id = ''
        Student.count += 1

    def printInfo(self):
        print(f'姓名：{self.name}')
        print(f'性别：{self.gender}')
        print(f'年龄：{self.age}')
        print(f'班级：{self.class_}')
        print(f'学号：{self.stu_id}')

    @classmethod
    def printStuCount(cls):
        print(f'学生总数：{cls.count}')


class Teacher(SchoolMem):
    count = 0

    def __init__(self):
        super().__init__()
        self.department = ''
        self.tea_id = ''
        Teacher.count += 1

    def printInfo(self):
        print(f'姓名：{self.name}')
        print(f'性别：{self.gender}')
        print(f'年龄：{self.age}')
        print(f'科室：{self.department}')
        print(f'工号：{self.tea_id}')

    @classmethod
    def printTeaCount(cls):
        print(f'教师总数：{cls.count}')


if __name__ == '__main__':
    # 测试各项属性
    stu = Student()
    stu.set_age(18)
    stu.set_gender('男')
    stu.set_name('张三')
    stu.class_ = '一班'
    stu.stu_id = '001'
    stu.printInfo()
    print()
    stu.printStuCount()
    print()

    tea = Teacher()
    tea.set_gender('女')
    tea.set_name('李四')
    tea.set_age(30)
    tea.department = '数学'
    tea.tea_id = '001'
    tea.printInfo()
    print()
    tea.printTeaCount()
    print()
    print('学生总数：', Student.count)
    print('教师总数：', Teacher.count)
