"""
编写函数，定义一个求Fibonacci数列的函数，并输出前20项，每行输出5项。
"""


def fibonacci(n):
    if n == 1 or n == 2:
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


if __name__ == "__main__":
    for i in range(1, 21):
        print(f"{fibonacci(i)}", end="\t")
        if i % 5 == 0:
            print()
    print()

