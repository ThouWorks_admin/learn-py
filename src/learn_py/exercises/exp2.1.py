"""
编写程序，输入5个整数放到列表list1中，输出下标及值，
然后将列表list1中大于平均值的元素组成一个新列表list2，输出平均值和列表list2。
请利用列表推导式解决该问题。
"""


def main():
    list1 = []
    for i in range(5):
        list1.append(int(input(f"Enter an integer for list1[{i}]: ")))

    print("list1:")
    for index, value in enumerate(list1):
        print(f"list1[{index}] = {value}")

    average = sum(list1) / len(list1)
    list2 = [x for x in list1 if x > average]
    print(f"average = {average}")

    print("list2:")
    for index, value in enumerate(list2):
        print(f"list2[{index}] = {value}")


if __name__ == "__main__":
    main()
