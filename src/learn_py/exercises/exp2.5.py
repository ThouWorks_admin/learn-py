"""
编写程序，创建一个字典，
要求是30以内的可以被2整除但不能被3整除的数，
将该数的平方按顺序加入字典。
"""

from pprint import pprint

if __name__ == "__main__":
    dict1 = {x: x**2 for x in range(2, 30) if x % 2 == 0 and x % 3 != 0}
    pprint(dict1)
