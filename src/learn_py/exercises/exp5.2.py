"""
分析交换机中的数据，如下，按照要求解析出数据，并保存到文本文件中。

Port priority: 0
Peak value of input: 34125767 bytes/sec, at 2000-04-26 14:07:55 
Peak value of output: 1536558 bytes/sec, at 2000-04-26 14:07:45 
Last 300 seconds input:720 packets/sec 994769 bytes/sec 1%
Last 300 seconds output:315 packets/sec27408 bytes/sec	0%	
Input (total):159235636 packets,222960253227 bytes
    159234524 unicasts,159 broadcasts,953 multicasts
Input (normal):159235636 packets,- bytes
    159234524 unicasts,159 broadcasts,953 multicasts
Input: 0 input errors, 0 runts, 0 giants,0 throttles
    0 CRC, 0 frame,- overruns, 0 aborts
    - ignored, - parity errors
Output (total): 139887609 packets,9299255264 bytes
    139880534 unicasts,1116 broadcasts,5959 multicasts, 0 pauses
Output (normal):139887609packets, - bytes
    139880534 unicasts,1116 broadcasts,5959 multicasts, 0 pauses
Output: 0 output errors, - underruns,- buffer failures
    0 aborts, 0 deferred, 0 collisions, 0 late collisions
    0 lost carrier, - no carrier

输入峰值速率（bytes/sec）：
输出峰值速率（bytes/sec）：
5分钟平均输入速率(packets/sec,bytes/sec)：
5分钟平均输出速率(packets/sec,bytes/sec)：
5分钟平均输入带宽利用率：
5分钟平均输出带宽利用率:
输入总包数：（packets）
输入总流量（bytes）：
输出总包数：（packets）
输出总流量（bytes）：
"""

import re


def main():
    data = """
    Port priority: 0
    Peak value of input: 34125767 bytes/sec, at 2000-04-26 14:07:55 
    Peak value of output: 1536558 bytes/sec, at 2000-04-26 14:07:45 
    Last 300 seconds input:720 packets/sec 994769 bytes/sec 1%
    Last 300 seconds output:315 packets/sec27408 bytes/sec	0%	
    Input (total):159235636 packets,222960253227 bytes
        159234524 unicasts,159 broadcasts,953 multicasts
    Input (normal):159235636 packets,- bytes
        159234524 unicasts,159 broadcasts,953 multicasts
    Input: 0 input errors, 0 runts, 0 giants,0 throttles
        0 CRC, 0 frame,- overruns, 0 aborts
        - ignored, - parity errors
    Output (total): 139887609 packets,9299255264 bytes
        139880534 unicasts,1116 broadcasts,5959 multicasts, 0 pauses
    Output (normal):139887609packets, - bytes
        139880534 unicasts,1116 broadcasts,5959 multicasts, 0 pauses
    Output: 0 output errors, - underruns,- buffer failures
        0 aborts, 0 deferred, 0 collisions, 0 late collisions
        0 lost carrier, - no carrier
    """
    pattern = re.compile(r'(\d+)')
    with open('data.txt', 'w') as f:
        f.write('输入峰值速率（bytes/sec）：' + pattern.findall(data)[0] + '\n')
        f.write('输出峰值速率（bytes/sec）：' + pattern.findall(data)[1] + '\n')
        f.write('5分钟平均输入速率(packets/sec,bytes/sec)：' + pattern.findall(data)
                [2] + ' ' + pattern.findall(data)[3] + '\n')
        f.write('5分钟平均输出速率(packets/sec,bytes/sec)：' + pattern.findall(data)
                [4] + ' ' + pattern.findall(data)[5] + '\n')
        f.write('5分钟平均输入带宽利用率：' + pattern.findall(data)[6] + '\n')
        f.write('5分钟平均输出带宽利用率:' + pattern.findall(data)[7] + '\n')
        f.write('输入总包数：（packets）' + pattern.findall(data)[8] + '\n')
        f.write('输入总流量（bytes）：' + pattern.findall(data)[9] + '\n')
        f.write('输出总包数：（packets）' + pattern.findall(data)[10] + '\n')
        f.write('输出总流量（bytes）：' + pattern.findall(data)[11] + '\n')


if __name__ == '__main__':
    main()
