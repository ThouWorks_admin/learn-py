"""
根据输入参数(行数)不同，输出下面图形
       *
      ***
     *****
    ********
"""


def draw_star(n):
    for i in range(n):
        print(' ' * (n - i - 1) + '*' * (2 * i + 1))


if __name__ == '__main__':
    line = int(input('请输入行数：'))
    draw_star(line)
