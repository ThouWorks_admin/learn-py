"""
编写程序，某家商店根据客户消费总额的不同将客户分为不同的类型。
如果消费总额>=10万，为铂金卡客户(platinum)；如果消费总额>=5万且小于10万，为金卡客户(gold) ；
如果消费总额>=3万且小于5万，为银卡客户（silver）；
如果消费总额<3万，为普卡客户(ordinary)。
现有一批顾客的消费金额（单位：万元）分别为：2.3、4.5、24、17、1、7.8、39、21、0.5、1.2、4、1、0.3，
将消费金额存储在列表list1中，
输出一个字典，分别以platinum、 gold、 silver、 ordinary为键，以各客户类型人数为值。
"""


if __name__ == "__main__":
    list1 = [2.3, 4.5, 24, 17, 1, 7.8, 39, 21, 0.5, 1.2, 4, 1, 0.3]
    dict1 = {
        "platinum": len([x for x in list1 if x >= 10]),
        "gold": len([x for x in list1 if 5 <= x < 10]),
        "silver": len([x for x in list1 if 3 <= x < 5]),
        "ordinary": len([x for x in list1 if x < 3])
    }

    print(dict1)
