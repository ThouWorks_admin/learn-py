# 创建文件hello.txt,写入内容“hello，world”，向文件“hello.txt”中追加从0到9的随机整数, 10个数字一行，共10行整数。

import random


def main():
    with open('hello.txt', 'w') as f:
        f.write('hello, world\n')
        for _ in range(10):
            for _ in range(10):
                f.write(str(random.randint(0, 9)) + ' ')
            f.write('\n')


if __name__ == '__main__':
    main()
