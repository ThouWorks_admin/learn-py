"""
定义两个函数，分别用于将小写字母转换为大写和将大写字母转换为小写。
"""


def lower_to_upper(s):
    return s.upper()


def upper_to_lower(s):
    return s.lower()


if __name__ == '__main__':
    s = "Hello, World!"
    print(f"lower_to_upper('{s}') = {lower_to_upper(s)}")
    print(f"upper_to_lower('{s}') = {upper_to_lower(s)}")
