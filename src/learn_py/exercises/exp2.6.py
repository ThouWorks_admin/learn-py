"""
编写程序，请输入20个字符串，
要求找出含a的字符串，并存在列表并输出，
请用列表推导式解决该问题。
"""


def main():
    list1 = [input(f"请输入第{i+1}个字符串：") for i in range(20)]
    list2 = [x for x in list1 if 'a' in x]
    print("list2:", list2)


if __name__ == "__main__":
    main()
