import sys
from pprint import pprint


def gen():
    for i in range(10):
        yield i*2


def fun1():
    x = 6

    def fun2():
        nonlocal x
        x *= x

        return x
    return fun2()


if __name__ == '__main__':
    test_str = ' + - * /'
    print(test_str.split(' '))

    tup1 = (1, 3, 5, 7)
    lst = list(tup1)
    print(lst)

    pprint(sorted(sys.modules.keys()))
    print(sys.modules.values())
    print(sys.modules['os'])
    pprint(sys.modules)

    # 匿名函数使用
    print((lambda x: x*x)(3))
    print((lambda x, y: x*y)(3, 4))

    pprint(list(gen()))
    pprint(fun1())
    list1 = ["key1", "key2", "key3"]
    list2 = [1, 2, 3]

    pprint({list1[i] for i in range(len(list1))})
