from pprint import pprint

import openpyxl


def read_xl(file_path):
    workbook = openpyxl.load_workbook(file_path)
    active_sheet = workbook.active

    print(workbook.sheetnames)
    pprint(list(active_sheet.values))

    for row in active_sheet.iter_rows():
        for cell in row:
            print(cell.value)


if __name__ == '__main__':
    read_xl('example.xlsx')
