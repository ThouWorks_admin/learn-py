from difflib import SequenceMatcher
from pprint import pprint

if __name__ == '__main__':
    src = open('./hello.txt', 'r', encoding='utf-8').read()
    des = open(
        './src/learn_py/exercises/hello.txt',
        'r',
        encoding='utf-8'
    ).read()

    differ = SequenceMatcher(lambda x: x == '', src, des).get_opcodes()

    for tag, i1, i2, j1, j2 in differ:
        print(tag, i1, i2, j1, j2)
        pprint(src[i1:i2])
        pprint(des[j1:j2])
        print()
